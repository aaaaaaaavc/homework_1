package dgut.homework;

import javax.json.bind.annotation.*;

public class StudentDTO {
    private int id;

    @JsonbProperty("姓名")
    private String name;

    @JsonbProperty("学号")
    private String identity;

    @JsonbTransient
    private int age;

    public StudentDTO() {

    }
    public StudentDTO(String name, String identity, int age) {
        this.name = name;
        this.identity = identity;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
