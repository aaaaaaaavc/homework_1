package dgut.homework;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.stream.Collectors;

@WebServlet(urlPatterns = {"/json"})
public class JsonServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        Jsonb jsonb = JsonbBuilder.create();
        //
        String postJsonString = new BufferedReader(new InputStreamReader(req.getInputStream()))
                .lines().collect(Collectors.joining(System.lineSeparator()));
        // 利用jsonb把json字符串转换为对象
        StudentDTO personDTO = jsonb.fromJson("{\"姓名\":\"yi\",\"学号\":\"201641404239\",\"age\":23}", StudentDTO.class);
        String jsonPerson = jsonb.toJson(personDTO);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().println(postJsonString+jsonPerson);
        //添加进h2
        String URL = "jdbc:h2:~/test;MODE=MYSQL;DB_CLOSE_DELAY=-1";
        String NAME = "sa";
        String PASSWORD = "sa";

        try (Connection conn = DriverManager.getConnection(URL, NAME, PASSWORD)) {
            //通过数据库的连接操作数据库，实现增删改查
            Statement stmt = conn.createStatement();

            // 创建一个表
            stmt.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS `user` (" +
                            "  `id` int(11) unsigned NOT NULL AUTO_INCREMENT," +
                            "  `name` varchar(100) NOT NULL," +
                            "  `identity` varchar(100) NOT NULL," +
                            "  `age` int(11) NOT NULL," +
                            "  PRIMARY KEY (`id`)" +
                            ");"
            );

            stmt.executeUpdate(
                    "insert into `user` (`name`,`identity`,`age`) values (" + personDTO.getName() + "," + personDTO.getIdentity() + "," + personDTO.getAge() + ");"
            );

            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");

            resp.getWriter().println(postJsonString);

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
